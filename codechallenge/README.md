# **Bienvenidos al repositorio de la prueba VueJs-Junior**

## **Tecnologias usadas**
- *VueJS*
- *Axios *

## **Introducción**

En el siguiente proyecto se plante el consumo del siguien EndPoint (*https://my-user-manager.herokuapp.com/users*) mediande VueJS y el cliente HTTP Axios. 

## **Despliegue del proyecto en GitHub Pages**

*[https://jaojedat.github.io/codechallenge.github.io/](https://jaojedat.github.io/codechallenge.github.io/)*

## **Despliegue en 000WebHost**

*[https://codechallenge-vue.000webhostapp.com/](https://codechallenge-vue.000webhostapp.com/)*